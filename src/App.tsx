import React from 'react';
import './App.css';
import { TimeTable } from './components/TimeTable/TimeTable';

function App() {
  return (
    <div className="App">
      <TimeTable />
    </div>
  );
}

export default App;
